from flask import Flask, render_template, request, jsonify
import yaml
import psycopg2
import json
import os
# from database import *

app = Flask(__name__)

import psycopg2

dbuser=os.environ['db_admin_user']
dbpass=os.environ['db_password']
dbhost=os.environ['db_server_name']
dbname=os.environ['db_name']
dbsslmode = os.environ['db_ssl_mode']

conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(dbhost, dbuser, dbname, dbpass, dbsslmode)
conn = psycopg2.connect(conn_string)
print("\nCUSTOMER_MICROSERVICE: connection established\n")

cursor = conn.cursor()

@app.route('/')
def index():
    return render_template('home.html')

@app.route('/customer/get-all', methods=['GET'])
def get_all():
    try:
        query = "SELECT id, first_name, middle_name, last_name, username, card_number, expiry_date, email, phone_number, address, city, region, country FROM customer;"
        cursor.execute(query)
        rows = cursor.fetchall()
        resp = jsonify(rows)
        resp.status_code = 200
        return resp
    except psycopg2.DatabaseError as err:
        conn.rollback()
        return {"message":"Database error: {0}".format(err)}, 500
    except Exception as err:
        return {"message":"Internal error: {0}".format(err)}, 500
	
@app.route('/customer/get-one/id/<string:id>', methods=['GET'])
def get_customer_by_id(id):
    try:
        query = "SELECT id, first_name, middle_name, last_name, username, card_number, expiry_date, email, phone_number, address, city, region, country FROM customer WHERE id = %s ;"
        cursor.execute(query,(id,))
        rows = cursor.fetchall()
        resp = jsonify(rows)
        resp.status_code = 200
        return resp
    except psycopg2.DatabaseError as err:
        conn.rollback()
        return {"message":"Database error: {0}".format(err)}, 500
    except Exception as err:
        return {"message":"Internal error: {0}".format(err)}, 500
    
@app.route('/customer/get-one/username/<string:username>', methods=['GET'])
def get_customer_by_username(username):
    try:
        query = "SELECT id, first_name, middle_name, last_name, username, card_number, expiry_date, email, phone_number, address, city, region, country FROM customer WHERE username=%s;"
        cursor.execute(query,(username,))
        rows = cursor.fetchall()
        resp = jsonify(rows)
        resp.status_code = 200
        return resp
    except psycopg2.DatabaseError as err:
        conn.rollback()
        return {"message":"Database error: {0}".format(err)}, 500
    except Exception as err:
        return {"message":"Internal error: {0}".format(err)}, 500

# INSERT REST API
	
@app.route('/customer/insert', methods=['POST'])
def insert_customer():
    try:
        input_json = request.get_json(force=True) 
        first_name = input_json['first_name']
        middle_name = input_json['middle_name']
        last_name = input_json['last_name']
        username = input_json['username']
        password = input_json['password']
        card_number = input_json['card_number']
        expiry_date = input_json['expiry_date']
        email = input_json['email']
        phone_number = input_json['phone_number']
        address = input_json['address']
        city = input_json['city']
        region = input_json['region']
        country = input_json['country']
        
        query = "INSERT INTO customer (first_name, middle_name, last_name, username, password, card_number, expiry_date, email, phone_number, address, city, region, country) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"
        cursor.execute(query,(first_name, middle_name, last_name, username, password, card_number, expiry_date, email, phone_number, address, city, region, country))
        conn.commit()
        return {"message":"Customer was inserted successfully"}, 200
    except psycopg2.DatabaseError as err:
        conn.rollback()
        return {"message":"Database error: {0}".format(err)}, 500
    except Exception as err:
        return {"message":"Internal error: {0}".format(err)}, 500
    
@app.route('/customer/login', methods=['POST'])
def is_loged():
    try:
        input_json = request.get_json(force=True) 
        username = input_json['username']
        password = input_json['password']
        
        query = "SELECT * FROM customer WHERE username=%s AND password=%s;"
        cursor.execute(query,(username, password))
        if cursor.rowcount == 1:
            return {"logged":True}, 200
        else:
            return {"logged":False}, 200
    except psycopg2.DatabaseError as err:
        conn.rollback()
        return {"message":"Database error: {0}".format(err)}, 500
    except Exception as err:
        return {"message":"Internal error: {0}".format(err)}, 500

@app.route('/customer/update/<string:id>', methods=['PUT'])
def update_customer_by_id(id):
    try:
        input_json = request.get_json(force=True) 
        first_name = input_json['first_name']
        middle_name = input_json['middle_name']
        last_name = input_json['last_name']
        username = input_json['username']
        card_number = input_json['card_number']
        expiry_date = input_json['expiry_date']
        email = input_json['email']
        phone_number = input_json['phone_number']
        address = input_json['address']
        city = input_json['city']
        region = input_json['region']
        country = input_json['country']
        
        query = "UPDATE customer SET first_name =%s, middle_name =%s, last_name =%s, username =%s, card_number =%s, expiry_date =%s, email =%s, phone_number =%s, address =%s, city =%s, region =%s, country =%s WHERE id =%s;"
        cursor.execute(query, (first_name, middle_name, last_name, username, card_number, expiry_date, email, phone_number, address, city, region, country, id))
        conn.commit()
        return {"message":"Customer was updated successfully"}, 200
    except psycopg2.DatabaseError as err:
        conn.rollback()
        return {"message":"Database error: {0}".format(err)}, 500
    except Exception as err:
        return {"message":"Internal error: {0}".format(err)}, 500

@app.route('/customer/delete/id/<string:id>', methods=['DELETE'])
def delete_customer_by_id(id):
    try:
        query = "DELETE FROM customer WHERE id = %s;"
        cursor.execute(query, (id,))
        return {"message":"Customer was deleted successfully"}, 200
    except psycopg2.DatabaseError as err:
        conn.rollback()
        return {"message":"Database error: {0}".format(err)}, 500
    except Exception as err:
        return {"message":"Internal error: {0}".format(err)}, 500
    
@app.route('/customer/delete/username/<string:username>', methods=['DELETE'])
def delete_customer_by_username(username):
    try:
        query = "DELETE FROM customer WHERE username = %s;"
        cursor.execute(query, (username,))
        return {"message":"Customer was deleted successfully"}, 200
    except psycopg2.DatabaseError as err:
        conn.rollback()
        return {"message":"Database error: {0}".format(err)}, 500
    except Exception as err:
        return {"message":"Internal error: {0}".format(err)}, 500

if __name__ == '__main__':
    app.run(host = '0.0.0.0',port =5000)