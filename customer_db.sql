CREATE DATABASE customer_db;

\c customer_db;

CREATE TABLE customer (
	id SERIAL NOT NULL ,
	first_name VARCHAR(30) NOT NULL,
	middle_name VARCHAR(30) NOT NULL,
	last_name VARCHAR(30) NOT NULL,
	username VARCHAR(30) NOT NULL UNIQUE,
	password VARCHAR(50) NOT NULL,
	card_number VARCHAR(6) NOT NULL,
	expiry_date VARCHAR(10) NOT NULL,
	email VARCHAR(100) NOT NULL,
	phone_number VARCHAR(15) NOT NULL,
	address varchar(100) NOT NULL,
	city VARCHAR(30) NOT NULL,
	region VARCHAR(20) NOT NULL,
	country VARCHAR(20) NOT NULL,
	PRIMARY KEY (id)
);

CREATE USER customer_db_user WITH ENCRYPTED PASSWORD 'palito_de_oro';
GRANT ALL PRIVILEGES ON DATABASE customer_db TO customer_db_user;
GRANT ALL PRIVILEGES ON TABLE customer TO customer_db_user;
GRANT ALL PRIVILEGES ON SEQUENCE customer_id_seq TO customer_db_user;