#! /usr/bin/env bash

echo "Insert a customer: "
curl -H "Content-Type:application/json" -X POST -d '{"first_name":"Joe", "middle_name":"Peter", "last_name":"Smith", "username":"jsmith", "password":"my_amazing_password", "card_number":"123456", "expiry_date":"06-2021", "email":"j_smith@gmail.com", "phone_number":"56912345678", "address":"23 Acacia Avenue", "city":"Chillan", "region":"nuble", "country":"chile"}' http://localhost:5000/customer/insert

echo "Consult for all customers"
curl http://localhost:5000/customers/all

echo "Insert another customer: "
curl -H "Content-Type:application/json" -X POST -d '{"first_name":"Alan", "middle_name":"Robert", "last_name":"Doe", "username":"Alan21", "password":"another_password123", "card_number":"654321", "expiry_date":"06-2021", "email":"alan_123@gmail.com", "phone_number":"56912345678", "address":"100 Ohiggins AV", "city":"Valparaiso", "region":"Valparaiso", "country":"chile"}' http://localhost:5000/customer/insert

echo "Insert another customer: "
curl -H "Content-Type:application/json" -X POST -d '{"first_name":"Marck", "middle_name":"Brad", "last_name":"Pattirson", "username":"ghwe3", "password":"my_top_secret_password", "card_number":"654321", "expiry_date":"06-2021", "email":"mark_xd@gmail.com", "phone_number":"56988888888", "address":"25 Merylan St.", "city":"Santiago", "region":"Metropolitana", "country":"chile"}' http://localhost:5000/customer/insert

echo "Ask for a login: "
curl -H "Content-Type:application/json" -X POST -d '{"username":"ghwe3", "password":"my_top_secret_password"}' http://localhost:5000/customer/login

echo "Updtate a customer: "
curl -H "Content-Type:application/json" -X PUT -d '{"first_name":"Juaquin", "middle_name":"Antonio", "last_name":"Mendez", "username":"HolaHola", "card_number":"654321", "expiry_date":"06-2021", "email":"alan_123@gmail.com", "phone_number":"56912345678", "address":"100 Ohiggins AV", "city":"Valparaiso", "region":"Valparaiso", "country":"chile"}' http://localhost:5000/customer/update/14

echo "Consult for all customers: "
curl http://localhost:5000/customers/all

echo "Delete a customer: "
curl -X DELETE http://localhost:5000/customer/1